FROM node

RUN mkdir /skillbox
WORKDIR /skillbox
COPY package.json /skillbox
RUN yarn install

COPY . /skillbox
RUN yarn test
RUN yarn build

#будет запускаться когда запускается контейнер docker run
CMD yarn start

# на каком порту слушается приложение
EXPOSE 3000
